package br.ufscar.dc.o3ctool.bean;

import br.ufscar.dc.o3ctool.dao.jpa.ProjectDAO;
import br.ufscar.dc.o3ctool.dao.jpa.ProjectJPA;
import br.ufscar.dc.o3ctool.model.Project;
import br.ufscar.dc.o3ctool.model.Researcher;
import br.ufscar.dc.o3ctool.util.Messages;
import br.ufscar.dc.o3ctool.util.Session;
import br.ufscar.dc.o3ctool.util.i18n;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "projbean")
@SessionScoped
public class ProjectBean implements Serializable {

    private Project proj;
    private final ProjectDAO projDAO;

    public ProjectBean() {
        proj = Session.getActivSettings().getProject();
        projDAO = new ProjectJPA();
    }

    public String backToProjects() {
        proj = Session.getActivSettings().getProject();
        return "projects.xhtml?faces-redirect=true";
    }

    public String changeLicense() {
        if (proj.isLicencePrivate()) {
            proj.setLicense(Project.ProjectLicense.PUBLIC);
        } else {
            proj.setLicense(Project.ProjectLicense.PRIVATE);
        }
        return saveProject();
    }

    public String saveProject() {
        Researcher activeResearcher = Session.getActivResearcher();
        if (proj.getId() == null) {
            if (projDAO.alreadyExistingProject(activeResearcher.getId(), proj)) {
                Messages.addMessage(
                        FacesMessage.SEVERITY_ERROR,
                        i18n.getProperty("err_de_project"),
                        null);
                return null;
            }
        }
        proj.setResearcher(activeResearcher);
        projDAO.save(proj);
        proj = Session.getActivSettings().getProject();
        return "projects.xhtml?faces-redirect=true";
    }

    public String removeProject() {
        try {
            projDAO.remove(proj);
            proj = Session.getActivSettings().getProject();
            return "projects.xhtml?faces-redirect=true";
        } catch (Exception ex) {
            Messages.addMessage(
                    FacesMessage.SEVERITY_ERROR,
                    i18n.getProperty("err_constraint_exception"),
                    null);
            return null;
        }
    }

    public List<Project> getAllProjects() {
        return projDAO.getAllProjects(Session.getActivResearcher().getId());
    }

    public Project getProj() {
        return proj;
    }

    public void setProj(Project proj) {
        this.proj = proj;
    }
}
