package br.ufscar.dc.o3ctool.bean;

import br.ufscar.dc.o3ctool.dao.jpa.RequirementDAO;
import br.ufscar.dc.o3ctool.dao.jpa.RequirementJPA;
import br.ufscar.dc.o3ctool.model.Project;
import br.ufscar.dc.o3ctool.model.Requirement;
import br.ufscar.dc.o3ctool.model.Researcher;
import br.ufscar.dc.o3ctool.util.Messages;
import br.ufscar.dc.o3ctool.util.Session;
import br.ufscar.dc.o3ctool.util.i18n;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

@ManagedBean(name = "reqbean")
@ViewScoped
public class RequirementBean implements Serializable {

    private Requirement req;
    private final RequirementDAO reqDAO;
    private Long requirementId;

    public RequirementBean() {
        req = Session.getActivSettings().getRequirement();
        reqDAO = new RequirementJPA();
        requirementId = null;
    }

    public String saveRequirement() {
        Project activeproject = Session.getActiveProject();
        if (req.getId() == null) {
            if (reqDAO.alreadyExistingRequirement(activeproject.getId(), req)) {
                Messages.addMessage(
                        FacesMessage.SEVERITY_ERROR,
                        i18n.getProperty("err_de_req"),
                        null);
                return null;
            }
        }
        req.setProject(activeproject);
        reqDAO.save(req);
        req = Session.getActivSettings().getRequirement();
        return "req.xhtml?faces-redirect=true";
    }

    public String removeRequirement() {
        try {
            reqDAO.remove(req);
            req = Session.getActivSettings().getRequirement();
            return "req.xhtml?faces-redirect=true";
        } catch (Exception ex) {
            Messages.addMessage(
                    FacesMessage.SEVERITY_ERROR,
                    i18n.getProperty("err_constraint_exception"),
                    null);
            return null;
        }
    }

    public List<Requirement> getAllRequirements() {
        return reqDAO.getAllRequirements(Session.getActiveProject().getId());
    }

    public Requirement getReq() {
        return req;
    }

    public void setReq(Requirement req) {
        this.req = req;
    }

    public Long getRequirementId() {
        return requirementId;
    }

    public void setRequirementId(Long requirementId) {
        this.requirementId = requirementId;
    }
}
